var logg= Ti.API.info;


Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
};

Ti.include('settings.js');
//Ti.include('list.js');

var Cloud = require('ti.cloud');
var win = Ti.UI.currentWindow;

var addreminderholder = Ti.UI.createView({
	top : '0dp',
	height : '70dp',
	backgroundColor : 'white',
});

var reminderfield = Ti.UI.createTextField(namefields);
reminderfield.top = "30dp";
reminderfield.hintText = "Do";
reminderfield.left = "10dp";
reminderfield.right = "30dp";
reminderfield.font = {
	fontSize : 18,
	fontFamily : 'HelveticaNeue-UltraLight'
};

reminderfield.addEventListener('focus', function(e) {
	reminderfield.value = '';
});

var saveButton = Ti.UI.createButton({
	top : "30dp",
	right : '10dp',
	style : Titanium.UI.iPhone.SystemButton.CONTACT_ADD,

});

var notesTableView = Ti.UI.createTableView({
	top : "80dp",
	bottom : "70dp",
	rowHeight : '50dp',
	editable : true,
	moveable : true,
	moving : true,
	separatorStyle : Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
});

var signedinuserlabel = Ti.UI.createLabel({
	top : "5dp",
	left : '10dp',
	text : '',
	font : {
		fontSize : 12,
		fontFamily : 'HelveticaNeue-UltraLight'
	},

});

if (Ti.App.Properties.getString('useremail') != null) {
	signedinuserlabel.text = '' + Ti.App.Properties.getString('useremail');
}

var platformwidth = Titanium.Platform.displayCaps.platformWidth;
var platformheight = Titanium.Platform.displayCaps.platformHeight;

var settingsview = Ti.UI.createView({
	top : '0dp',
	bottom : '70dp',
	left : '0dp',
	backgroundColor : '#ffffff',

});

var signinusername = Ti.UI.createTextField(namefields);
signinusername.top = '130dp';
signinusername.hintText = 'Email';
signinusername.value = '';
signinusername.font = {
	fontSize : 16,
	fontFamily : 'HelveticaNeue-UltraLight'
};
signinusername.autocapitalization = Ti.UI.TEXT_AUTOCAPITALIZATION_NONE;

var signinpassword = Ti.UI.createTextField(namefields);
signinpassword.top = '160dp';
signinpassword.hintText = 'Password';
signinpassword.value = '';
signinpassword.font = {
	fontSize : 16,
	fontFamily : 'HelveticaNeue-UltraLight'
};

var signinbtn = Ti.UI.createButton(userbuttons);
signinbtn.top = '190dp';
signinbtn.title = 'Sign in';

var signupusername = Ti.UI.createTextField(namefields);
signupusername.top = '20dp';
signupusername.hintText = 'Email';
signupusername.value = '';
signupusername.font = {
	fontSize : 16,
	fontFamily : 'HelveticaNeue-UltraLight'
};

var signuppassword = Ti.UI.createTextField(namefields);
signuppassword.top = '50dp';
signuppassword.hintText = 'Password';
signuppassword.value = '';
signuppassword.font = {
	fontSize : 16,
	fontFamily : 'HelveticaNeue-UltraLight'
};

var signuppasswordconfirmation = Ti.UI.createTextField(namefields);
signuppasswordconfirmation.top = '80dp';
signuppasswordconfirmation.hintText = 'Confirm password';
signuppasswordconfirmation.value = '';
signuppasswordconfirmation.font = {
	fontSize : 16,
	fontFamily : 'HelveticaNeue-UltraLight'
};

var signupbtn = Ti.UI.createButton(userbuttons);
signupbtn.top = '110dp';
signupbtn.title = 'Sign up';

var showuserbtn = Ti.UI.createButton(userbuttons);
showuserbtn.top = '250dp';
showuserbtn.title = 'Show user';

var logoutuserbtn = Ti.UI.createButton(userbuttons);
logoutuserbtn.top = '230dp';
//logoutuserbtn.right = '10dp',
logoutuserbtn.title = 'Log out';

/*
var resetbtn = Ti.UI.createButton(userbuttons);
resetbtn.top = '290dp';
resetbtn.title = 'Reset password';
*/

///////SIGN UP//////
var readers = {
	publicAccess : false,
	ids : []
};

var writers = {
	publicAccess : false,
	ids : []
};

signupbtn.addEventListener('click', function(e) {
	Ti.App.Properties.setString('useremail', signinusername.value);
	signupusername.blur();
	signuppassword.blur();
	Cloud.Users.create({
		email : signupusername.value,
		password : signuppassword.value,
		password_confirmation : signuppasswordconfirmation.value,
		first_name : signupusername.value,
		last_name : signupusername.value,

	}, function(e) {
		if (e.success) {
			var user = e.users[0];
			
			Ti.App.Properties.setString('userid', user.id);
			Ti.App.Properties.setString('useremail', signinusername.value);
			Ti.App.Properties.setString('userpassword', signinpassword.value);
			settingsview.add(logoutuserbtn);

			win.add(signedinuserlabel);
			signedinuserlabel.text = '';
            logg(user.id);
//parent=signinusername.value;
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}

	});

});



///////SIGN IN//////

signinbtn.addEventListener('click', function(e) {
	signinusername.blur();
	signinpassword.blur();

	Cloud.Users.login({
		login : signinusername.value,
		password : signinpassword.value,
	}, function(e) {
		if (e.success) {

			var user = e.users[0];
			//Ti.App.Properties.removeProperty('userid');
			//Ti.App.Properties.removeProperty('useremail');
			//Ti.App.Properties.setString('sessionID',e.meta.session_id);
			Ti.App.Properties.setString('userid', user.id);
			Ti.App.Properties.setString('useremail', signinusername.value);
			Ti.App.Properties.setString('userpassword', signinpassword.value);
			signedinuserlabel.text = '' + Ti.App.Properties.getString('useremail');
			settingsview.add(logoutuserbtn);
			win.add(signedinuserlabel);
			getData();

			//alert('Success:\n' + 'id: ' + user.id + '\n' + 'sessionId: ' + Cloud.sessionId + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

/////SHOW USER////
showuserbtn.addEventListener('click', function(e) {
	Cloud.Users.showMe(function(e) {
		if (e.success) {
			var user = e.users[0];
			//alert('Success:\n' + 'id: ' + user.id + '\n' + 'first name: ' + user.first_name + '\n' + 'id name: ' + user.id + '\n' + 'last name: ' + user.last_name);
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

/////LOGOUT USER////

logoutuserbtn.addEventListener('click', function(e) {
	Cloud.Users.logout(function(e) {
		if (e.success) {
			//Ti.App.Properties.removeProperty('sessionID');
			// Ti.App.Properties.removeProperty('useremail');
			// Ti.App.Properties.removeProperty('userpassword');
			// Ti.App.Properties.removeProperty('userid');
			settingsview.remove(logoutuserbtn);

			signedinuserlabel.text = '' + Ti.App.Properties.getString('useremail'), alert('Success: Logged out');
			alert(Ti.App.Properties.getString('userid'));
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

/////RESET PASSWORD////
/*
resetbtn.addEventListener('click', function(e){
Cloud.Users.requestResetPassword({
email: 'bj.wiking@gmail.com'
}, function (e) {
if (e.success) {
alert('Success: Reset Request Sent');
} else {
alert('Error:\n' +
((e.error && e.message) || JSON.stringify(e)));
}
});
});

*/

win.add(addreminderholder);
addreminderholder.add(reminderfield);
addreminderholder.add(saveButton);

settingsview.add(signupusername);
settingsview.add(signuppassword);
settingsview.add(signuppasswordconfirmation);
settingsview.add(signupbtn);

settingsview.add(signinusername);
settingsview.add(signinpassword);
settingsview.add(signinbtn);
//settingsview.add(showuserbtn);
//settingsview.add(logoutuserbtn);
win.add(notesTableView);
win.add(signedinuserlabel);
//win.add(settingsview);

//win.add(resetbtn);

/////TOOLBAR////

var reloadButton = Ti.UI.createButton({
	//title: "Ladda om",
	//bottom: "20dp",
	//left: '50dp',
	systemButton : Titanium.UI.iPhone.SystemButton.REFRESH,

});

var closebtn = Ti.UI.createButton({
	//title: "Ladda om",
	bottom : "20dp",
	//left: '50dp',
	systemButton : Titanium.UI.iPhone.SystemButton.TRASH,

});
var loggParentbtn = Ti.UI.createButton({
	title: "Ladda förälder",
	top : "40dp",
	width: '100dp',
	
	left:0
	//systemButton : Titanium.UI.iPhone.SystemButton.TRASH,

});







win.add(loggParentbtn);


loggParentbtn.addEventListener("click", function(e) {
	//Ti.App.Properties.setString('parent', 'Förälder');
	
	Cloud.Users.showMe(function (e) {
    if (e.success) {
        var user = e.users[0];
        alert('Success:\n' +
            'id: ' + user.id + '\n' +
            'first name: ' + user.first_name + '\n' +
            'last name: ' + user.last_name);
    } else {
        alert('Error:\n' +
            ((e.error && e.message) || JSON.stringify(e)));
    }
});
});



closebtn.addEventListener("click", function(e) {
	win.remove(settingsview);
});




var settingsButton = Ti.UI.createButton({
	//title: "Ladda om",
	//bottom: "20dp",
	//left: '50dp',
	systemButton : Titanium.UI.iPhone.SystemButton.COMPOSE,

});

settingsButton.addEventListener("click", function(e) {
	win.add(settingsview);
});

reloadButton.addEventListener("click", function(e) {
	getData();
});

var space = Ti.UI.createButton({
	systemButton : Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE,
	//width: '140dp',
});

var toolbar = Titanium.UI.iOS.createToolbar({
	items : [closebtn, space, reloadButton, space, settingsButton],
	bottom : '0dp',
	left : '0dp',
	//width: '50dp',
	borderTop : false,
	borderBottom : false,
	barColor : '#fffff',
});

win.add(toolbar);

/////MAKE ROWS///
var notesTableData = [];
var personLabel = '';
var rowData = [];
function makeRow(eventsid, note, bgColor) {
	row = Ti.UI.createTableViewRow({
		height : "50dp",
		objectId : eventsid,
		backgroundColor : bgColor,
		note: note,
		rownumber: notesTableData.length
	});

	personLabel = Ti.UI.createLabel({
		text : note,
		left : "10dp",
		font : {
			fontSize : 22,
			fontFamily : 'HelveticaNeue-UltraLight'
		},
	});
	row.add(personLabel);

	notesTableData.push(row);
}

///////GET DATA/////

//alert(signedinuserid);

function getData() {
	// var signedinuserid = Ti.App.Properties.getString('userid');
	
	notesTableData = [];

	Cloud.Objects.query({
		classname : 'events',
		order : 'objectnumber',
		 where : {
			// //acl_id: '527a2f09bfed710590001e6b',
			//user_id : signedinuserid,
		//event : 'aaaa'
		 }

	}, function(e) {
		if (e.success) {
			//alert('Success:\n' +
			//    'Count: ' + e.events.length);
			notesTableData = [];
			for (var i = 0; i < e.events.length; i++) {
				var events = e.events[i];
				makeRow(events.id, events.event, events.color);

				// alert('id: ' + events.id + '\n' +
				//     'make: ' + events.event + '\n' +
				//     'color: ' + events.color + '\n' +
				// 'year: ' + events.year + '\n' +
				//   'created_at: ' + events.created_at);
			}
			notesTableView.setData(notesTableData);

		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
}

getData();

/////CHECK SIGNED IN AND  GET DATA////
if (Ti.App.Properties.getString('useremail') != null) {
	//Cloud.sessionId = Ti.App.Properties.getString('sessionID');
	alert(Ti.App.Properties.getString('useremail'));
	alert(Ti.App.Properties.getString('userpassword'));
	Cloud.Users.login({
		login : Ti.App.Properties.getString('useremail'),
		password : Ti.App.Properties.getString('userpassword'),
	}, function(e) {
		if (e.success) {
			getData();

		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
}

///////SETDATA/////

////SKAPA OBJECTNUMBER NÄR POST SKAPAS, UPPDATERA ANDRA POSTER MED DERAS NYA OBJEKTSNUMMER!!!!!

saveButton.addEventListener("click", function(e) {
	
	logg(Ti.App.Properties.getString('useremail'));
	reminderfield.blur();
	Cloud.Objects.create({
		//acl_name : useracl,
		classname : 'events',
		fields : {
			event : reminderfield.value,
			// event : Ti.App.Properties.getString('parent'),
			color : 'white',
			objectnumber :'',
			//year: 2005,	
		}
		
	}, function(e) {
		if (e.success) {
			var event = e.events[0];
			getData();
			//alert('Success:\n' + 'id: ' + event.id + '\n' + 'event: ' + event.event + '\n' + 'color: ' + event.color + '\n' + 'year: ' + event.year + '\n' + 'created_at: ' + event.created_at);
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

///////DELETEROWS/////

notesTableView.addEventListener("delete", function(e) {
	//Ti.API.info(e.rowData.objectId);
	//var rowid = e.rowData.note;
	//alert(e.rowData);
	Cloud.Objects.remove({
		classname : 'events',
		id : e.rowData.objectId,

	}, function(e) {

		if (e.success) {
			alert('Success');
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

///////SEND ROW ORDER/////
//var rows = tableview.data[0].rows;

notesTableView.addEventListener("move", function(e) {
	Ti.API.info(e.index);
	
	notesTableData.move(e.rowData.rownumber, e.index);
	
	for(i = 0;i < notesTableData.length;i++)
	{
		notesTableData[i].rownumber = i;
		Ti.API.info(notesTableData[i].note);
	}
	
	updateRows(0);
	
});


function updateRows(counter)
{
	if(counter == notesTableData.length)
	{
		getData();
		return;
	}
	
	
	Cloud.Objects.update({
		classname : 'events',
		id : notesTableData[counter].objectId,//e.rowData.objectId,
		fields : {
			objectnumber: counter //e.index,
		}

	}, function(e) {
		Ti.API.info("WE ARE DONE "+counter);
		if (e.success) {
			var event = e.events[0];
			//alert('Success:\n' + 'id: ' + event.id + '\n' + 'event: ' + event.event + '\n' + 'color: ' + event.color + '\n' + 'year: ' + event.year + '\n' + 'created_at: ' + event.created_at);
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
		updateRows(counter+1);
	});
	
}

